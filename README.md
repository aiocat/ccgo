# ccgo

Minimal, easy-to-use tool for compile your go app cross-platform.

## Short Description

ccgo (**C**ross-platform **C**ompile for **Go**) is a simple and minimal tool for compile your go app for **14** different systems only with one command.

## Installation

Get the latest binaries from release page and add to your path.

## Usage

Make sure you initialized your go.mod file before using this tool. (`go mod init <name>`)

### Simple Usage

- `ccgo example` (arg1 is the name of the app.)

It will compile and save all of the files into the `.ccgo` folder.

### Advanced Usage

Now with `v0.1.0` ccgo also supports custom configs. Let's create `ccgo.config.json` for make a simple config. For list of the configs:

### Config Types

- os (`string array`): List of the operating systems.
  - default: `["freebsd", "windows", "linux", "openbsd", "darwin"]`
- arch (`string array`): List of the architectures.
  - default: `["amd64", "386", "arm64"]`
- folder (`string`): The folder that will contain compiled files.
  - default: `".ccgo"`

#### Added in v0.2.0

- checksum (`bool`): Should ccgo create a checksum file for compiled files.
  - default: `false`
- zip (`bool`): Should compress the folders with zip format.
  - default: `false`
- hash (`string`): Hash algorithm for checksum (only works if checksum is enabled).
  - default: `"sha256"`
  - supported: `"sha256"`, `"sha1"`, `"md5"` (will set to the default value if the value is not in this list.)

An example config (`ccgo.config.json`):

```json
{
  "os": ["linux", "windows", "darwin"],
  "arch": ["amd64", "386"],
  "folder": "builds",
  "checksum": true,
  "hash": "md5",
  "zip": true
}
```

Now when we run ccgo, it will only compile for these operating systems - archs and will save to the `./builds` folder.

## Compiling

If you don't have any pre-built binaries, you can easily build the ccgo yourself.

### Before Continue...

Make sure you installed Go.

### Step-by-Step

- Download the source code (`git clone https://gitlab.com/aiocat/ccgo.git`)
- Go to the directory (`cd ccgo`)
- Run: `go get github.com/logrusorgru/aurora/v3` (This package is used for colored output.)
- Compile the program with: `go build .`

Now you have an executable ccgo file.

## Found a bug? Got an error?

- Please create a new issue on gitlab repository if you found a bug or an error.

## Contributing

If you want to contribute this project:

- Please use go-fmt for formatting.
- Make sure you add the comments for your codes.
- Please do not something useless.

## Authors

- [Aiocat](https://gitlab.com/aiocat)

## License

This project is distributed under GPLv3 license.

## Project status

Under development.
