package main

import (
	"fmt"
	"os"
	"os/exec"
	"time"

	"github.com/logrusorgru/aurora/v3"
)

func main() {
	var config Config

	// Read the config
	parseConfig(&config)

	// Check if got an error
	if config.Error != nil {
		errorAndExit("Config Error", config.Error.Error())
	}

	// Check go.mod is not exists
	if _, err := os.Stat("go.mod"); os.IsNotExist(err) {
		errorAndExit("Base Error", "can't find go.mod file, please initialize the project with:\n  go mod init <name>")
	}

	// Load operating systems
	operatingSystems, archs := config.loadOsAndArch()

	// Set compile folder
	compileFolder := config.setCompileFolder()

	// Create compile folder
	os.Mkdir(compileFolder, os.ModePerm)

	// Start compiling
	startTime := time.Now()
	success, failed := config.compilePrograms(getName(), compileFolder, operatingSystems, archs)

	// Print output
	outputStatus(success, failed, startTime)
}

// Compile programs.
func (config Config) compilePrograms(name, compileFolder string, operatingSystems, archs []string) ([]string, []string) {
	var success, failed []string

	// Iterate operating systems.
	for _, sys := range operatingSystems {
		// Iterate archs.
		for _, arch := range archs {
			// Skip if darwin/386
			if sys == "darwin" && arch == "386" {
				continue
			}

			osAndArch := sys + "/" + arch
			fmt.Printf("%s Compiling for %s\n", colorizeText("⟳", aurora.BrightBlue, aurora.Bold), colorizeText(osAndArch, aurora.Underline, aurora.BrightWhite))

			// Create directory
			dirName := name + "-" + sys + "-" + arch
			os.Mkdir(compileFolder+"/"+dirName, os.ModePerm)

			// Set environment variables
			os.Setenv("GOOS", sys)
			os.Setenv("GOARCH", arch)

			// Build the binary
			tempName := name

			if sys == "windows" {
				tempName += ".exe"
			}

			dirPath := "./" + compileFolder + "/" + dirName
			fullPath := dirPath + "/" + tempName
			_, err := exec.Command("go", "build", "-ldflags", "-s -w", "-o", fullPath).CombinedOutput()

			// Check error(s)
			if err != nil {
				fmt.Printf("%s Compiling %s is failed (%s)\n", colorizeText("✘", aurora.BrightRed, aurora.Bold), colorizeText(osAndArch, aurora.Underline, aurora.BrightWhite), err.Error())
				failed = append(failed, osAndArch)
				os.RemoveAll(dirPath)
				continue
			} else {
				fmt.Printf("%s Compiled %s\n", colorizeText("✔", aurora.BrightGreen, aurora.Bold), colorizeText(osAndArch, aurora.Underline, aurora.BrightWhite))
			}

			// Write checksum if exists
			if config.Checksum {
				data, err := os.ReadFile(fullPath)

				if err != nil {
					errorAndExit("Unknown Error", err.Error())
				}

				os.WriteFile(fullPath+"."+config.Hash, config.loadChecksum(data), 0666)
			}

			// Create zip file if added
			if config.Zip {
				createZip(dirPath, dirPath+".zip")
			}

			success = append(success, osAndArch)
		}
	}

	return success, failed
}
