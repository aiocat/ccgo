package main

// Config struct
type Config struct {
	Error    error `json:"-"`
	Os, Arch []string
	Folder   string
	Checksum bool
	Hash     string
	Zip      bool
}
