package main

import (
	"archive/zip"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/logrusorgru/aurora/v3"
)

// Parse json and edit the config.
func parseConfig(conf *Config) {
	if _, err := os.Stat("ccgo.config.json"); os.IsNotExist(err) {
		return
	}

	data, err := os.ReadFile("ccgo.config.json")

	if err != nil {
		conf.Error = err
		return
	}

	err = json.Unmarshal(data, conf)
	conf.Error = err
}

// Parse cli return the app name.
func getName() string {
	args := os.Args[1:]

	if len(args) < 1 {
		errorAndExit("Argument Error", "can't find name argument.")
	}

	return args[0]
}

// Print colored
func colorizeText(text string, colors ...func(arg interface{}) aurora.Value) string {
	val := aurora.Reset(text)

	// Iterate over colors.
	for i := 0; i < len(colors); i++ {
		val = colors[i](val)
	}

	return val.String()
}

// Print an error and exit
func errorAndExit(title, text string) {
	fmt.Printf("Error (%s): %s\n", colorizeText(title, aurora.BrightRed, aurora.Bold), colorizeText(text, aurora.BrightWhite))
	os.Exit(1)
}

// Print success, failed values.
func outputStatus(success, failed []string, t time.Time) {
	boldArrow := colorizeText("↪", aurora.BrightWhite, aurora.Bold)

	fmt.Printf("%s Compiling is finished\n%s Total %s platform\n%s Elapsed %s\n  %s Success (%d):\n",
		colorizeText("★", aurora.BrightYellow, aurora.Bold),
		boldArrow,
		colorizeText(strconv.Itoa(len(success)+len(failed)), aurora.BrightMagenta, aurora.Bold),
		boldArrow,
		colorizeText(time.Since(t).String(), aurora.BrightMagenta, aurora.Bold),
		colorizeText("✔", aurora.BrightGreen, aurora.Bold),
		len(success),
	)

	// Iterate over success
	for i := 0; i < len(success); i++ {
		fmt.Printf("    %s\n", colorizeText(success[i], aurora.BrightGreen))
	}

	fmt.Printf("  %s Failed (%d):\n", colorizeText("✘", aurora.BrightRed, aurora.Bold), len(failed))

	// Iterate over failed
	for i := 0; i < len(failed); i++ {
		fmt.Printf("    %s\n", colorizeText(failed[i], aurora.BrightRed))
	}
}

// Load checksum for data.
func (c *Config) loadChecksum(data []byte) []byte {
	var checksumHex string

	if c.Hash == "sha256" {
		checksum := sha256.New().Sum(data)
		checksumHex = fmt.Sprintf("%x", checksum)
	} else if c.Hash == "sha1" {
		checksum := sha1.Sum(data)
		checksumHex = fmt.Sprintf("%x", checksum)
	} else if c.Hash == "md5" {
		checksum := md5.Sum(data)
		checksumHex = fmt.Sprintf("%x", checksum)
	} else {
		c.Hash = "sha256"
		checksum := sha256.New().Sum(data)
		checksumHex = fmt.Sprintf("%x", checksum)
	}

	return []byte(checksumHex)
}

// Load operating systems / archs
func (c Config) loadOsAndArch() ([]string, []string) {
	var osystems, archs []string

	if len(c.Os) < 1 {
		osystems = []string{"freebsd", "windows", "linux", "openbsd", "darwin"}
	} else {
		osystems = c.Os
	}

	// Load archs
	if len(c.Arch) < 1 {
		archs = []string{"amd64", "386", "arm64"}
	} else {
		archs = c.Arch
	}

	return osystems, archs
}

// Set compile folder
func (c Config) setCompileFolder() string {
	var compileFolder string

	if c.Folder == "" {
		compileFolder = ".ccgo"
	} else {
		compileFolder = c.Folder
	}

	return compileFolder
}

// Simple zip utility for ccgo.
func createZip(input, output string) error {
	newZipFile, err := os.Create(output)

	if err != nil {
		return err
	}
	defer newZipFile.Close()

	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()

	// Get files.
	folders, err := os.ReadDir(input)

	if err != nil {
		return err
	}

	// Add files to zip file.
	for _, file := range folders {
		filename := filepath.Join(input, file.Name())

		fileToZip, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer fileToZip.Close()

		// Get the file information.
		info, err := fileToZip.Stat()
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		header.Name = filename
		header.Method = zip.Deflate

		writer, err := zipWriter.CreateHeader(header)
		if err != nil {
			return err
		}

		io.Copy(writer, fileToZip)
	}

	return nil
}
